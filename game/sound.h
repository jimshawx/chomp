//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

enum class Sfx
{
	pill,
	powerpill,
	spawnin,
	dead,
	caughtGhost,

	lastSfx
};

void playSfx(Sfx fx);
void startMusic(void);
void stopMusic(void);
void musicVolume(unsigned int vol);
void soundInit(void);
void soundShutdown(void);
