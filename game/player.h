//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include <vector>

#include "grid.h"
#include "dbgnew.h"

class Ghost;
enum class gameover
{
	GameInProgress,
	AllLivesLost,
	ExitGame,
};

class Player
{
public:
	Player(Grid *g)
	{
		grid = g;
		int px, py;
		grid->start(px, py);
		playerX = (float)px;
		playerY = (float)py;
		setState(playerstate::SpawningIn);
	}

	~Player()
	{
		ghosts.clear();
	}

	void draw();
	void move(int dt);
	void getGridPosition(int &x, int &y);
	void getPixelPosition(float &x, float &y);
	void getTarget(int &x, int &y);
	void caughtByGhost();
	void caughtGhost();
	void addGhost(Ghost *g);
	void resetGhostMultiplier();
	unsigned int getScore() { return score; }
	unsigned int getLives() { return lives; }
	void drawDying();
	void drawSpawning();
	void setStatus(unsigned int s, unsigned int l)
	{
		score = s;
		lives = l;
	}
	gameover gameOver();
private:
	Grid *grid = nullptr;
	float playerX = 0.0f;
	float playerY = 0.0f;
	float dirX = -1.0f;
	float dirY = 0.0f;
	const float playerSpeed = 1.0f;
	const float speedScale = 0.006f;
	//tweak based on maximum ghost speed
	const float turnTolerance = 0.15f;

	unsigned int score = 0;
	unsigned int lives = 3;
	const int startingGhostMultiplier = 200;
	unsigned int ghostMultiplier = startingGhostMultiplier;
	vector<Ghost *> ghosts;

	enum class playerstate
	{
		SpawningIn,
		Normal,
		Dying,
		Frozen
	};

	playerstate state = playerstate::SpawningIn;

	void drawNormal();
	void moveNormal(int dt);
	void setState(playerstate state);

	int spawnInTimer = 0;
	const int spawnInTime = 4000;
	
	int frozenTimer = 0;
	const int frozenTime = 5000;
	
	int dyingTimer = 0;
	const int dyingTime = 2000;
};
