//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include "sprite.h"
#include "gamemode.h"

class HiScore : public GameMode
{
public:
	HiScore();
	~HiScore();

	virtual void init();
	virtual void shutdown();

	virtual void draw3d();
	virtual void drawhud();

	virtual gamemode update(int dt);
private:
	Sprite *font = nullptr;
};
