//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------

#include <windows.h>
#include <gl/gl.h>

#include <cmath>

#include "attract.h"
#include "gamemode.h"
#include "utils.h"

using namespace std;

extern void drawcapsule();

extern const int SCREEN_WIDTH;

Attract::Attract()
{
	font = new Sprite(L"gfx/font.bmp", true);
}

Attract::~Attract()
{
	if (font != nullptr)
	{
		delete font;
		font = nullptr;
	}
}

void Attract::init()
{
}

void Attract::shutdown()
{
}

void Attract::draw3d()
{
	float xx = -400;
	float yy = 200;
	float zz = -1000;

	//red
	glColor3ub(255, 0, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(xx, yy, zz);
	glScalef(50.0f, 50.0f, 50.0f);
	drawcapsule();
	drawEyes();
	glPopMatrix();

	yy -= 100;

	//pink
	glColor3ub(253, 153, 203);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();	glTranslatef(xx, yy, zz);
	glScalef(50.0f, 50.0f, 50.0f);
	drawcapsule();
	drawEyes();
	glPopMatrix();

	yy -= 100;

	//cyan
	glColor3ub(0, 255, 255);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(xx, yy, zz);
	glScalef(50.0f, 50.0f, 50.0f);
	drawcapsule();
	drawEyes();
	glPopMatrix();

	yy -= 100;

	//orange
	glColor3ub(255, 128, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(xx, yy, zz);
	glScalef(50.0f, 50.0f, 50.0f);
	//glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	drawcapsule();
	drawEyes();
	glPopMatrix();
}

void Attract::drawEyes()
{
	float xx, yy, zz;
	glBegin(GL_QUADS);

	zz = 0.7f;
	float eye = 0.3f;
	float pupil = 0.2f;

	glColor3f(1.0f, 1.0f, 1.0f);

	xx = -0.5f + (0.5f - eye) / 2.0f;
	yy = 0.25f;
	glVertex3f(xx, yy, zz);
	glVertex3f(xx + eye, yy, zz);
	glVertex3f(xx + eye, yy + eye, zz);
	glVertex3f(xx, yy + eye, zz);

	xx = 0.0f + (0.5f - eye) / 2.0f;
	glVertex3f(xx, yy, zz);
	glVertex3f(xx + eye, yy, zz);
	glVertex3f(xx + eye, yy + eye, zz);
	glVertex3f(xx, yy + eye, zz);

	glColor3f(0.0f, 0.0f, 0.0f);

	float offx = 0.5f - pupil;
	//if (dirX < 0.0f) offx = 0.0f;
	float offy = eye - pupil;
	//if (dirY > 0.0f) offy = 0.0f;

	xx = -0.5f + offx;
	yy = 0.25f + offy;
	glVertex3f(xx, yy, zz);
	glVertex3f(xx + pupil, yy, zz);
	glVertex3f(xx + pupil, yy + pupil, zz);
	glVertex3f(xx, yy + pupil, zz);

	xx = 0.0f + offx;
	glVertex3f(xx, yy, zz);
	glVertex3f(xx + pupil, yy, zz);
	glVertex3f(xx + pupil, yy + pupil, zz);
	glVertex3f(xx, yy + pupil, zz);

	glEnd();
}
void Attract::drawhud()
{
	glColor3f(1.0f, 1.0f, 1.0f);

	int x1 = font->extentString("CHARACTER / NICKNAME");
	font->drawString((SCREEN_WIDTH - x1) / 2, 50, "CHARACTER / NICKNAME");
	glColor3ub(255, 0, 0);

	int xx = 400;
	int yy = 175;
	int ys = 85;
	font->drawString(xx, yy, "SHADOW      \"BLINKY\""); yy += ys;
	glColor3ub(253, 153, 203);
	font->drawString(xx, yy, "SPEEDY      \"PINKY\""); yy += ys;
	glColor3ub(0, 255, 255);
	font->drawString(xx, yy, "BASHFUL     \"INKY\""); yy += ys;
	glColor3ub(255, 128, 0);
	font->drawString(xx, yy, "POKEY       \"CLYDE\""); yy += ys;

	float t0 = rainbowTimer;
	int i0, i1;
	float f0;
	static const float rainbow[] = {
		1.0f,0.0f,0.0f,
		1.0f,0.5f,0.0f,
		1.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,1.0f,
		0.5f,0.0f,1.0f,
		1.0f,0.0f,1.0f,
	};
	float i;
	f0 = modf(t0, &i);

	i0 = (int)t0 % 7;
	i1 = (i0 + 1) % 7;
	float dr = rainbow[i0 * 3] + f0 * (rainbow[i1 * 3] - rainbow[i0 * 3]);
	float dg = rainbow[i0 * 3+1] + f0 * (rainbow[i1 * 3+1] - rainbow[i0 * 3+1]);
	float db = rainbow[i0 * 3+2] + f0 * (rainbow[i1 * 3+2] - rainbow[i0 * 3+2]);

	glColor3f(dr,dg,db);
	int x0 = font->extentString("PRESS SPACE TO START!");

	font->drawString((SCREEN_WIDTH - x0) / 2, 550, "PRESS SPACE TO START!");
}

gamemode Attract::update(int dt)
{
	rainbowTimer += (float)dt * rainbowSpeed;

	if (GetAsyncKeyState(VK_SPACE))
		return gamemode::Play;

	return gamemode::Attract;
}
