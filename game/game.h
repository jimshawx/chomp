//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include <windows.h>

#include <vector>

#include "render.h"
#include "grid.h"
#include "ghost.h"
#include "player.h"
#include "sprite.h"
#include "gamemode.h"
#include "attract.h"
#include "hiscore.h"
#include "play.h"

#include "dbgnew.h"

using namespace std;

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

class Game
{
public:
	Game(HWND hwnd, HINSTANCE hinstance)
	{
		gameWindow = hwnd;
		gameInstance = hinstance;
	}
	~Game();

	int init(void);

	int run(int dt);

	void switchMode(gamemode mode);

private:
	HWND gameWindow = nullptr;
	HINSTANCE gameInstance = nullptr;
	Render *render = nullptr;

	gamemode mode = gamemode::Attract;
	GameMode *state = nullptr;

	Attract *attract = nullptr;
	Play *play = nullptr;
	HiScore *hiscore = nullptr;
};

