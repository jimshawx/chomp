//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

void seed(void);
float drand(void);
float drand(float scale);
float drand(float start, float end);
int irand(void);

