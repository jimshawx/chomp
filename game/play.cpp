//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------

#include "play.h"
#include "gamemode.h"

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

Play::Play()
{
	font = new Sprite(L"gfx/font.bmp", true);
}

Play::~Play()
{
	if (font)
	{
		delete font;
		font = nullptr;
	}
}

void Play::init()
{
	grid = new Grid();
	grid->load("grid0.txt");

	player = new Player(grid);

	Blinky *blinky = new Blinky(grid, player);
	ghosts.push_back(new Inky(grid, player, blinky));
	ghosts.push_back(blinky);
	ghosts.push_back(new Pinky(grid, player));
	ghosts.push_back(new Clyde(grid, player));

	for (auto g : ghosts)
		player->addGhost(g);
}

void Play::shutdown()
{
	if (grid)
	{
		delete grid;
		grid = nullptr;
	}

	for (auto g : ghosts)
		delete g;
	ghosts.clear();

	if (player)
	{
		delete player;
		player = nullptr;
	}
}

void Play::nextGrid()
{
	unsigned int score = player->getScore();
	unsigned int lives = player->getLives();
	shutdown();
	init();
	player->setStatus(score, lives);
}

void Play::draw3d()
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -(float)SCREEN_WIDTH * 1.2f);
	glRotatef(-50.0f, 1.0f, 0.0f, 0.0f);

	grid->draw();

	for (auto g : ghosts)
		g->draw();

	player->draw();

	glPopMatrix();
}

void Play::drawhud()
{
	glColor4ub(255, 255, 255, 255);

	unsigned int score = player->getScore();
	unsigned int highScore = 3333360;

	int x = SCREEN_WIDTH / 2 - (SCREEN_HEIGHT * 3 / 4);

	int x0 = font->extentString("1 UP");
	int x1 = font->extentString("%8u0", score / 10);
	int x2 = font->extentString("HIGH SCORE");
	int x3 = font->extentString("%8u0", highScore / 10);

	font->drawString(x + (x1 - x0) / 2, 40, "1 UP");
	font->drawString(x, 80, "%8u0", score / 10);

	font->drawString(SCREEN_WIDTH / 2 - x2 / 2, 40, "HIGH SCORE");
	font->drawString(SCREEN_WIDTH / 2 - x3 / 2, 80, "%8u0", highScore / 10);

	if (player->gameOver() == gameover::AllLivesLost)
	{
		int x4 = font->extentString("GAME OVER!");
		font->drawString(SCREEN_WIDTH / 2 - x4 / 2 + (irand()%9)-4, SCREEN_HEIGHT/2-20 + (irand() % 9) - 4, "GAME OVER!");
	}

	unsigned int lives = player->getLives();
	assert(lives <= 3);
	if (lives > 1)
	{
		lives--;
		char txt[5] = " @ @";
		if (lives == 1) txt[0] = txt[1] = ' ';

		glColor3ub(255, 255, 0);
		int x5 = font->extentString("%4s", txt);
		font->drawString(x, SCREEN_HEIGHT - 60, txt);
	}
}

gamemode Play::update(int dt)
{
	player->move(dt);

	for (auto g : ghosts)
		g->move(dt);

	if (player->gameOver() == gameover::ExitGame)
		return gamemode::Attract;

	if (grid->complete())
		nextGrid();

	return gamemode::Play;
}
