//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------

#include <windows.h>
#include <gl/gl.h>

#include "HiScore.h"
#include "gamemode.h"
#include "utils.h"

extern void drawcapsule();

extern const int SCREEN_WIDTH;

HiScore::HiScore()
{
	font = new Sprite(L"gfx/font.bmp", true);
}

HiScore::~HiScore()
{
	if (font != nullptr)
	{
		delete font;
		font = nullptr;
	}
}

void HiScore::init()
{
}

void HiScore::shutdown()
{
}

void HiScore::draw3d()
{
	float xx = -300;
	float yy = 200;
	float zz = -1000;

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	//orange
	glColor3ub(255, 128, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glTranslatef(xx, yy, zz);
	glScalef(50.0f, 50.0f, 50.0f);
	//glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	drawcapsule();
	glPopMatrix();

	yy -= 100;

	//pink
	glColor3ub(253, 153, 203);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();	glTranslatef(xx, yy, zz);
	glScalef(50.0f, 50.0f, 50.0f);

	//glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	drawcapsule();
	glPopMatrix();

	yy -= 100;
	//red
	glColor3ub(255, 0, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(xx, yy, zz);
	glScalef(50.0f, 50.0f, 50.0f);
	//glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	drawcapsule();
	glPopMatrix();

	yy -= 100;
	//cyan
	glColor3ub(0, 255, 255);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(xx, yy, zz);
	glScalef(50.0f, 50.0f, 50.0f);
	//glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	drawcapsule();
	glPopMatrix();
}

void HiScore::drawhud()
{
	glColor3f(1.0f, 1.0f, 0.0f);
	int x0 = font->extentString("PRESS SPACE TO START!");

	font->drawString((SCREEN_WIDTH - x0) / 2, 500, "PRESS SPACE TO START!");
}

gamemode HiScore::update(int dt)
{
	if (GetAsyncKeyState(VK_SPACE))
		return gamemode::Play;

	return gamemode::HiScore;
}
