//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include "grid.h"
#include "player.h"
#include "ghost.h"
#include "sprite.h"
#include "gamemode.h"

class Play : public  GameMode
{
public:
	Play();
	~Play();

	virtual void init();
	virtual void shutdown();

	virtual void draw3d();
	virtual void drawhud();

	virtual gamemode update(int dt);
private:
	Grid *grid = nullptr;
	vector<Ghost *> ghosts;
	Player *player = nullptr;

	Sprite *font = nullptr;

	void nextGrid();
};
