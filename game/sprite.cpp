#include <windows.h>
#include <objidl.h>

#pragma warning(push)
#pragma warning(disable:4458)
#include <gdiplus.h>
#pragma warning(pop)
#include <gl/gl.h>

#include <varargs.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "sprite.h"

Sprite::Sprite(const wchar_t *filename, bool trans)
{
	Gdiplus::Bitmap *bmp = Gdiplus::Bitmap::FromFile(filename, false);

	assert(bmp != nullptr);

	Gdiplus::BitmapData bmd;
	bmp->LockBits(NULL, Gdiplus::ImageLockModeRead, PixelFormat32bppARGB, &bmd);

	bool upsideDown = bmd.Stride < 0;
	if (upsideDown) bmd.Stride = -bmd.Stride;

	unsigned int *src = (unsigned int *)bmd.Scan0;

	/* fill in the Sprite data */
	unsigned int *dst = pix = new unsigned int[(size_t)bmd.Width * bmd.Height];
	width = bmd.Width;
	height = bmd.Height;
	istrans = trans;

	for (unsigned int y = 0; y < bmd.Height; y++)
	{
		memcpy(dst, src, sizeof(unsigned int) * bmd.Width);
		dst += bmd.Width;
		src += bmd.Stride / sizeof(unsigned int);
	}

	bmp->UnlockBits(&bmd);

	delete bmp;

	if (upsideDown)
	{
		unsigned int *line0 = pix;
		unsigned int *lineN = pix + ((size_t)height - 1) * width;
		unsigned int *tmpln = new unsigned int[width];
		for (int y = 0; y < height / 2; y++)
		{
			memcpy(tmpln, line0, width * sizeof * pix);
			memcpy(line0, lineN, width * sizeof * pix);
			memcpy(lineN, tmpln, width * sizeof * pix);
			line0 += width;
			lineN -= width;
		}
		delete[] tmpln;
	}

	if (trans)
	{
		unsigned int *p = pix;
		int len = width * height;
		while (len--)
		{
			unsigned int v = *p;
			*p++ = ((((v & 0xff) + ((v & 0xff00) >> 8) + ((v & 0xff0000) >> 16)) / 3) << 24) | (v & 0x00ffffff);
		}
	}

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glPixelStorei(GL_UNPACK_ROW_LENGTH, width);
	glTexImage2D(GL_TEXTURE_2D,
		0, 4,
		width, height,
		0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, pix);
}

Sprite::~Sprite()
{
	if (pix != nullptr)
	{
		delete[] pix;
		pix = nullptr;
	}
}

void Sprite::setTexture()
{
	glBindTexture(GL_TEXTURE_2D, texture);

	if (istrans)
	{
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
	}
	else
	{
		glDisable(GL_BLEND);
	}
}

void Sprite::draw(int x, int y)
{
	glEnable(GL_TEXTURE_2D);
	setTexture();

	glBegin(GL_QUADS);
		glTexCoord2i(0, 0);
		glVertex3i(x, y, 0);
		glTexCoord2i(0, 1);
		glVertex3i(x, y + height, 0);
		glTexCoord2i(1, 1);
		glVertex3i(x + width, y + height, 0);
		glTexCoord2i(1, 0);
		glVertex3i(x + width, y, 0);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

void Sprite::drawString(int x, int y, const char *fmt, ...)
{
	static char txt[1000];

	glEnable(GL_TEXTURE_2D);
	setTexture();
	
	va_list args;
	va_start(args, fmt);
	vsprintf_s(txt,1000,fmt, args);
	va_end(args);

	char *c = const_cast<char *>(txt);
	while (*c != '\0')
	{
		drawChar(*c++, x, y);
		x += 29;
	}

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

int Sprite::extentString(const char *fmt, ...)
{
	static char txt[1000];
	va_list args;
	va_start(args, fmt);
	vsprintf_s(txt, 1000, fmt, args);
	va_end(args);
	return (int)strlen(txt) * 29;
}

void Sprite::drawChar(char c, int x, int y)
{
	c -= ' ';

	//todo: these are fixed
	const int charsPerRow = 21;
	const int rowsOfChars = 5;

	assert(c >= 0 && c < charsPerRow *rowsOfChars);

	int charWidth = width / charsPerRow;
	int charHeight = height / rowsOfChars;

	int row = c / charsPerRow;
	int column = c % charsPerRow;

	float x0 = (float)(column * charWidth) / (float)width;
	float y0 = (float)(row * charHeight) / (float)height;

	float x1 = x0 + (float)charWidth / (float)width;
	float y1 = y0 + (float)charHeight / (float)height;

	glBegin(GL_QUADS);
		glTexCoord2f(x0, y0);
		glVertex3i(x, y, 0);
		glTexCoord2f(x0, y1);
		glVertex3i(x, y + charHeight, 0);
		glTexCoord2f(x1, y1);
		glVertex3i(x + charWidth, y + charHeight, 0);
		glTexCoord2f(x1, y0);
		glVertex3i(x + charWidth, y, 0);
	glEnd();
}
