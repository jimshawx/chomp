//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include <windows.h>
#include <gl/gl.h>
#include "glext.h"

#include "dbgnew.h"

class Render
{
public:
	Render(HWND hwnd)
	{
		gameWindow = hwnd;
	}
	~Render()
	{
		shutdown();
	}
	int init();
	void flip();
	void cls();
	void mode3d();
	void modeHud();
private:
	HGLRC gameRC=nullptr;
	HWND gameWindow;
	void (APIENTRY *wglSwapIntervalEXT)(int) = nullptr;

	void defaultstate();
	void shutdown();
};

