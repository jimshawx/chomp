//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include <windows.h>
#include <gl/gl.h>
#include <assert.h>
#include <ctype.h>

#include <cmath>
#include <string>

#include "grid.h"
#include "player.h"
#include "utils.h"
#include "logger.h"
#include "dbgnew.h"

class Ghost
{
	enum class ghoststate;
public:
	Ghost(Grid *g, Player *p)
	{
		grid = g;
		player = p;

		setState(ghoststate::Waiting);
	}
	virtual ~Ghost() = default;

	void draw();
	void move(int dt);
	void playerCollectedPowerPill()
	{
		setState(ghoststate::Frightened);
	}

	void playerDied()
	{
		setState(ghoststate::Scatter);
	}

	void playerRestarted()
	{
		setState(ghoststate::Scatter);
	}

private:
	virtual void decideChase() = 0;
	virtual void decideScatter() = 0;

	bool followTrack(int dt, float speed);

	const float normalSpeed = 1.0f;
	const float returningSpeed = 1.5f;
	const float ghostSpeed = 0.5f;
	const float waitingSpeed = 0.5f;
	const float treacleSpeed = 0.5f;

	const float speedScale = 0.006f;
	//tweak based on maximum ghost speed
	const float gridTolerance = 0.15f;

	void moveScatter(int dt);
	void moveChase(int dt);
	void moveWaiting(int dt);
	void moveFrightened(int dt);
	void moveReturning(int dt);

	void drawFrightened();
	void drawReturning();
	void drawEyes();

	bool needToFlipDirection = false;
	void flipDirection()
	{
		needToFlipDirection = true;
	}

	void applyFlipDirection()
	{
		dirX = -dirX;
		dirY = -dirY;
		updateTarget();
		needToFlipDirection = false;
	}

	void setState(ghoststate newstate);
	void updateState(int dt);

	void decideReturning();
	void decideFrightened();

	void collide();

	enum class ghoststate
	{
		Scatter,
		Chase,
		Frightened,
		Returning,
		Waiting,
		Freeze
	};

	ghoststate state = ghoststate::Scatter;

	const int waitTime = 4500;
	const int ghostTime = 5000;
	const int scatterTime = 5000;
	const int chaseTime = 20000;

protected:
	int frightenedTimer = 0;
	int waitTimer = 0;
	int scatterTimer = 0;
	int chaseTimer = 0;

	float ghostX = 0.0f;
	float ghostY = 0.0f;
	int targetX = 0;
	int targetY = 0;

	float dirX = 0.0f;
	float dirY = 0.0f;

	Grid *grid = nullptr;
	Player *player = nullptr;

	void updateTarget();
	void decideRandom();

	void headForPosition(int targetX, int targetY);
	virtual void drawNormal();
	virtual string getName() = 0;
	virtual void getWaitingHome(int &x, int &y) = 0;
};


class Blinky;
class Inky : public Ghost
{
public:
	Inky(Grid *g, Player *p, Blinky *b) : Ghost(g,p)
	{
		int homeX, homeY;
		getWaitingHome(homeX, homeY);
		ghostX = (float)homeX;
		ghostY = (float)homeY;

		blinky = b;
	};

protected:
	void drawNormal();
	void decideChase();
	void decideScatter();
	string getName();
	void getWaitingHome(int &x, int &y);

private:
	string name = "Inky";
	Blinky *blinky = nullptr;
};

class Blinky : public Ghost
{
public:
	Blinky(Grid *g, Player *p) : Ghost(g,p)
	{
		int homeX, homeY;
		getWaitingHome(homeX, homeY);
		ghostX = (float)homeX;
		ghostY = (float)homeY;

		//move off immediately
		waitTimer = 0;
	};

protected:
	void drawNormal();
	void decideChase();
	void decideScatter();
	string getName();
	void getWaitingHome(int &x, int &y);

private:
	string name = "Blinky";
};

class Pinky : public Ghost
{
public:
	Pinky(Grid *g, Player *p) : Ghost(g, p)
	{
		int homeX, homeY;
		getWaitingHome(homeX, homeY);
		ghostX = (float)homeX;
		ghostY = (float)homeY;
	};

protected:
	void drawNormal();
	void decideChase();
	void decideScatter();
	string getName();
	void getWaitingHome(int &x, int &y);

private:
	string name = "Pinky";
};

class Clyde : public Ghost
{
public:
	Clyde(Grid *g, Player *p) : Ghost(g,p)
	{
		int homeX, homeY;
		getWaitingHome(homeX, homeY);
		ghostX = (float)homeX;
		ghostY = (float)homeY;
	};

protected:
	void drawNormal();
	void decideChase();
	void decideScatter();
	string getName();
	void getWaitingHome(int &x, int &y);

private:
	string name = "Clyde";
};

