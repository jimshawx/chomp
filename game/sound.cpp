//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#include <windows.h>
#include <xaudio2.h>

#include <stdio.h>
#include <string.h>

#include <assert.h>

#include "ufmod.h"
#include "sound.h"

#include <crtdbg.h>
#include "dbgnew.h"

static IXAudio2 *xaudio;
static IXAudio2MasteringVoice *xaudioMasteringVoice;

class SoundFX
{
private:
	IXAudio2SourceVoice *xaudioSourceVoice = nullptr;
	XAUDIO2_BUFFER xaudioBuffer = { 0 };
	char *buffer = nullptr;
public:
	~SoundFX()
	{
		xaudioSourceVoice->DestroyVoice();
		xaudioSourceVoice = nullptr;
		delete [] buffer;
		buffer = nullptr;
	}

	SoundFX(const char *sfxname)
	{
		HRESULT err;
		long filelen;

		{
			FILE *f;
			fopen_s(&f, sfxname, "rb");
			if (f == NULL) return;
			fseek(f, 0, SEEK_END);
			filelen = ftell(f);
			buffer = new char[filelen];
			fseek(f, 0, SEEK_SET);
			fread(buffer, filelen, 1, f);
			fclose(f);
		}

		WAVEFORMATEX *wex = nullptr;
		BYTE *sfx = nullptr;
		char *bptr = buffer + 12;
		while (wex == nullptr || sfx == nullptr)
		{
			if (bptr + 4 >= buffer + filelen) break;

			unsigned int len;
			len = *(unsigned int *)(bptr + 4);
			if (!strncmp(bptr, "fmt ", 4))
			{
 				wex = (WAVEFORMATEX *)(bptr + 8);
			}
			else if (!strncmp(bptr, "data", 4))
			{
				sfx = (BYTE *)(bptr + 4);
				xaudioBuffer.Flags = 0;
				xaudioBuffer.AudioBytes = len;
				xaudioBuffer.pAudioData = sfx;
				xaudioBuffer.PlayBegin = 0;
				xaudioBuffer.PlayLength = len / (wex->nChannels * (wex->wBitsPerSample/8));
				xaudioBuffer.LoopBegin = 0;
				xaudioBuffer.LoopLength = 0;
				xaudioBuffer.LoopCount = 0;
				xaudioBuffer.pContext = nullptr;
			}
			bptr += len + 8ULL;
		}

		err = xaudio->CreateSourceVoice(&xaudioSourceVoice, wex);
		assert(err == S_OK);
	}

	void Play()
	{
		HRESULT err;
		err = xaudioSourceVoice->SubmitSourceBuffer(&xaudioBuffer, NULL);
		assert(err == S_OK);
		err = xaudioSourceVoice->Start();
		assert(err == S_OK);
	}
};

static SoundFX *sfxptr[(int)Sfx::lastSfx] = { 0 };

void soundInit()
{
	HRESULT err;
	err = XAudio2Create(&xaudio, 0, XAUDIO2_DEFAULT_PROCESSOR);
	assert(err == S_OK);

	err = xaudio->CreateMasteringVoice(&xaudioMasteringVoice, XAUDIO2_DEFAULT_CHANNELS, XAUDIO2_DEFAULT_SAMPLERATE, 0, nullptr, nullptr, AudioCategory_GameEffects);
	assert(err == S_OK);

	XAUDIO2_DEBUG_CONFIGURATION pdb;
	pdb.TraceMask = XAUDIO2_LOG_ERRORS | XAUDIO2_LOG_WARNINGS | XAUDIO2_LOG_DETAIL | XAUDIO2_LOG_API_CALLS;
	pdb.BreakMask = 0;// XAUDIO2_LOG_ERRORS;
	pdb.LogThreadID = true;
	pdb.LogFileline = true;
	pdb.LogFunctionName = true;
	pdb.LogTiming = true;
	xaudio->SetDebugConfiguration(&pdb, nullptr);

	//is this wise?
	waveOutSetVolume(nullptr, 0x7fff);
}

void soundShutdown()
{
	for (int i = 0; i < (int)Sfx::lastSfx; i++)
		if (sfxptr[i]) delete sfxptr[i];

	xaudioMasteringVoice->DestroyVoice();
	xaudio->Release();
}

void playSfx(Sfx fx)
{
	const char *sfx = "";

	switch (fx)
	{
		case Sfx::pill:
		case Sfx::powerpill:
			sfx = "music/sfx/gulp.wav";
			break;
		case Sfx::spawnin:
			sfx = "music/sfx/start.wav";
			break;
		case Sfx::dead:
			sfx = "music/sfx/death.wav";
			break;
		case Sfx::caughtGhost:
			sfx = "music/sfx/ghost.wav";
			break;
		default:
			assert(0);
	}

	/*
	char tmp[256];
	MCIERROR err;
	sprintf_s(tmp, 256, "play sfx%d from 0", fx);
	err = mciSendString(tmp, nullptr, 0, nullptr);
	if (err != 0)
	{
		sprintf_s(tmp, 256, "open \"%s\" type waveaudio alias sfx%d", sfx, fx);
		err = mciSendString(tmp, nullptr, 0, nullptr);
		assert(err == 0);

		sprintf_s(tmp, 256, "play sfx%d from 0", fx);
		err = mciSendString(tmp, nullptr, 0, nullptr);
		assert(err == 0);
	}
	*/
	/*
	PlaySound(sfx, NULL, SND_FILENAME | SND_ASYNC | SND_NOSTOP);
	*/

	if (sfxptr[(int)fx] == NULL)
		sfxptr[(int)fx] = new SoundFX(sfx);
	sfxptr[(int)fx]->Play();
}

void startMusic()
{
	//uFMOD_PlaySong((void *)"music/fast_mongo.xm", nullptr, XM_FILE);
	uFMOD_PlaySong((void *)"music/madness_final.xm", nullptr, XM_FILE);
}

void stopMusic()
{
	uFMOD_PlaySong(nullptr, nullptr, 0);
}

void musicVolume(unsigned int vol)
{
	uFMOD_SetVolume(vol);
}
