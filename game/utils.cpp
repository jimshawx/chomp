//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#include <stdlib.h>
#include <time.h>

void seed(void)
{
	srand((unsigned int)time(NULL));
}

float drand(void)
{
	return (float)rand() / (float)RAND_MAX;
}

float drand(float scale)
{
	return scale * drand();
}

float drand(float start, float end)
{
	return start + drand(end - start);
}

int irand(void)
{
	return rand();
}
