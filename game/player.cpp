//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#include <windows.h>
#include <gl/gl.h>

#include <cmath>

#include <assert.h>

#include "player.h"
#include "ghost.h"
#include "grid.h"
#include "sound.h"

#include "logger.h"
#include "dbgnew.h"

using namespace std;

extern void drawball();

void Player::draw()
{
	switch (state)
	{
		case playerstate::Normal:
			drawNormal();
			break;
		case playerstate::Frozen:
			drawDying();
			break;
		case playerstate::SpawningIn:
			drawSpawning();
			break;
		case playerstate::Dying:
			drawDying();
			break;
		default:
			assert(0);
	}
}

void Player::drawDying()
{
	float s = (float)dyingTimer / (float)dyingTime;
	float xy = 1.0f;
	if (s < 0.2f)
	{
		s = fmax(0.0f, s);
		xy = s * 5.0f;
		s = 0.2f;
	}

	float xx, yy, zz;
	grid->toXYZ(playerX, playerY, xx, yy, zz);

	xx += 15.0;
	yy += 15.0;

	glColor3ub(255, 255, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(xx + 12.5f, yy + 12.5f, zz + 50.0f);
	glScalef(50.0f*xy, 50.0f*xy, 50.0f*s);
	drawball();
	glPopMatrix();
}

void Player::drawSpawning()
{
	float s = (float)spawnInTimer / (float)spawnInTime;
	if (s < 0.0f) s = 0.0f;
	s = 1.0f - s;

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glScalef(1.0f, 1.0f, s);
	drawNormal();
	glPopMatrix();
}

void Player::drawNormal()
{
	

	float xx, yy, zz;
	grid->toXYZ(playerX, playerY, xx, yy, zz);

	xx += 15.0;
	yy += 15.0;

	glColor3ub(255, 255, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(xx+12.5f, yy+12.5f, zz+50.0f);
	glScalef(50.0f, 50.0f, 50.0f);
	drawball();
	glPopMatrix();
}

void Player::move(int dt)
{
	switch (state)
	{
		case playerstate::Normal:
			moveNormal(dt);
			break;

		case playerstate::Frozen:
			frozenTimer -= dt;
			break;

		case playerstate::SpawningIn:
			spawnInTimer -= dt;
			if (spawnInTimer < 0)
				setState(playerstate::Normal);
			break;

		case playerstate::Dying:
			dyingTimer -= dt;
			if (dyingTimer < 0)
			{
				lives--;
				if (lives == 0)
				{
					setState(playerstate::Frozen);
				}
				else
				{
					int px, py;
					grid->start(px, py);
					playerX = (float)px;
					playerY = (float)py;

					for (auto g : ghosts)
						g->playerRestarted();
					
					setState(playerstate::Normal);
				}
			}
			break;

		default:
			assert(0);
	}
}

gameover Player::gameOver()
{
	if (state == playerstate::Frozen && lives == 0)
	{
		if (frozenTimer < 0)
			return gameover::ExitGame;
		return gameover::AllLivesLost;
	}
	return gameover::GameInProgress;
}

void Player::moveNormal(int dt)
{
	playerX += (float)dt * dirX * playerSpeed * speedScale;
	playerY += (float)dt * dirY * playerSpeed * speedScale;

	int cellX = (int)playerX;
	int cellY = (int)playerY;

	int collected = grid->collect(cellX, cellY);
	if (collected == 1)
	{
		score += 10;
		playSfx(Sfx::pill);
	}
	else if (collected == 2)
	{
		score += 50;
		playSfx(Sfx::pill);
		for (auto g : ghosts)
			g->playerCollectedPowerPill();
	}

	//check we're not moving into a wall
	int wallX = cellX;
	int wallY = cellY;

	if (dirX == 1.0f) wallX++;
	if (dirY == 1.0f) wallY++;

	//is it a wall?
	if (grid->isWall(wallX, wallY))
	{
		//deal with the wall

		//back up the player to the grid cell
		playerX = (float)(wallX - dirX);
		playerY = (float)(wallY - dirY);

		cellX = (int)playerX;
		cellY = (int)playerY;

		dirX = 0.0f;
		dirY = 0.0f;
	}

	//is it hyperspace?
	int hx, hy;
	if (grid->isHyperspace(wallX, wallY, hx, hy))
	{
		playerX = (float)hx + dirX;
		playerY = (float)hy + dirY;
		return;
	}

	assert(!grid->isWall(cellX, cellY));

	int count;
	unsigned int routemask = grid->junctions(cellX, cellY, count);
	assert(count >= 2);

	unsigned int directions = 0;
	directions |=  ((GetAsyncKeyState('Q') | GetAsyncKeyState(VK_UP)) & 0x8000) != 0;
	directions |= (((GetAsyncKeyState('A') | GetAsyncKeyState(VK_DOWN)) & 0x8000) != 0) << 2;
	directions |= (((GetAsyncKeyState('O') | GetAsyncKeyState(VK_LEFT)) & 0x8000) != 0) << 3;
	directions |= (((GetAsyncKeyState('P') | GetAsyncKeyState(VK_RIGHT)) & 0x8000) != 0) << 1;

	//bool log = directions != 0;
	//static int i = 0;
	//if (log) elog("\n%d\nkeys %1X\n", i++, directions);

	//if we're going L<->R then need to be near edge of cell to change to U<->D
	if (dirX != 0.0f && fabs(playerX - truncf(playerX)) > turnTolerance) directions &= 2 + 8;

	//if we're going U<->D then need to be near edge of cell to change to L<->R
	if (dirY != 0.0f && fabs(playerY - truncf(playerY)) > turnTolerance) directions &= 1 + 4;

	//if (log) elog("restricted keys %1X\n", directions);

	directions &= routemask;

	if (directions != 0)
	{
		//if (log) elog("change dir %1X\n", directions);

		float newdirX = 0.0f;
		float newdirY = 0.0f;

		if (directions != 1 && directions != 2 && directions != 4 && directions != 8)
		{
			//pressing too many keys at the same time, pick one...
			//todo: what should be the key priority?
			if (directions & 1) directions = 1;
			if (directions & 2) directions = 2;
			if (directions & 4) directions = 4;
			if (directions & 8) directions = 8;
		}

		if (directions == 1) newdirY = -1.0f;
		if (directions == 2) newdirX = 1.0f;
		if (directions == 4) newdirY = 1.0f;
		if (directions == 8) newdirX = -1.0f;

		//if we changed direction, clamp the other component to the grid
		//if (dirX == 0.0f && newdirX != 0.0f) playerX = (float)cellX;
		//if (dirY == 0.0f && newdirY != 0.0f) playerY = (float)cellY;
		if (dirX == 0.0f && newdirX != 0.0f) playerY = (float)cellY;
		if (dirY == 0.0f && newdirY != 0.0f) playerX = (float)cellX;

		dirX = newdirX;
		dirY = newdirY;
	}
}

void Player::getGridPosition(int &x, int &y)
{
	x = (int)playerX;
	y = (int)playerY;
}

void Player::getPixelPosition(float &x, float &y)
{
	x = playerX;
	y = playerY;
}

void Player::getTarget(int &x, int &y)
{
	//find the next junction or corner the player is heading for 
	x = (int)playerX;
	y = (int)playerY;
	do
	{
		int nx, ny;
		nx = x + (int)dirX;
		ny = y + (int)dirY;
		
		if (grid->isWall(nx, ny))
			break;

		x = nx;
		y = ny;
	} while (!grid->isJunction(x, y) && !grid->isCorner(x, y));
}

void Player::caughtByGhost()
{
	if (state == playerstate::Frozen)
		return;

	if (state != playerstate::Dying)
	{
		for (auto g : ghosts)
			g->playerDied();

		setState(playerstate::Dying);
	}
}

void Player::setState(playerstate newState)
{
	state = newState;

	switch (state)
	{
		case playerstate::SpawningIn:
			playSfx(Sfx::spawnin);
			spawnInTimer = spawnInTime;
			break;
		case playerstate::Normal:
			break;
		case playerstate::Frozen:
			frozenTimer = frozenTime;
			break;
		case playerstate::Dying:
			playSfx(Sfx::dead);
			dyingTimer = dyingTime;
			break;
		default:
			assert(0);
	}
}

void Player::addGhost(Ghost *ghost)
{
	ghosts.push_back(ghost);
}

void Player::caughtGhost()
{
	playSfx(Sfx::caughtGhost);
	score += ghostMultiplier;
	ghostMultiplier *= 2;
}

void Player::resetGhostMultiplier()
{
	ghostMultiplier = startingGhostMultiplier;
}