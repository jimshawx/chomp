//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#include <windows.h>
#include <stdio.h>

#include "logger.h"

#define MAX_LOG_MSG (10*1024)

void elog(const char *fmt, ...)
{
	char tmp[MAX_LOG_MSG];
	va_list v;
	va_start(v, fmt);
	vsprintf_s(tmp, MAX_LOG_MSG, fmt, v);
	va_end(v);
	OutputDebugString(tmp);
}

void elog(LOG_LEVEL level, const char *fmt, ...)
{
	char tmp[MAX_LOG_MSG];
	va_list v;
	va_start(v, fmt);
	vsprintf_s(tmp, MAX_LOG_MSG, fmt, v);
	va_end(v);
	if (level == LOG_LEVEL::LOG_WARN) OutputDebugString("*** WARNING *** ");
	else if (level == LOG_LEVEL::LOG_ERROR) OutputDebugString("*** ERROR! *** ");
	OutputDebugString(tmp);
}