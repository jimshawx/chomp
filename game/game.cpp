//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#include <windows.h>
#include <gl/gl.h>

#include <assert.h>

#include "game.h"
#include "logger.h"
#include "dbgnew.h"
#include "attract.h"
#include "hiscore.h"
#include "play.h"
#include "sound.h"

int Game::run(int dt)
{
	assert(dt != 0);

	//when we're debugging (or game gets minimised), need to clamp this
	if (dt > 20) dt = 20;

	render->mode3d();
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	render->cls();

	state->draw3d();

	render->modeHud();

	state->drawhud();

	render->flip();

	gamemode newMode = state->update(dt);
	if (newMode != mode)
		switchMode(newMode);

	return 0;
}

Game::~Game()
{
	stopMusic();

	if (state != nullptr)
	{
		state->shutdown();
		state = nullptr;
	}

	if (render)
	{
		delete render;
		render = nullptr;
	}
	if (attract)
	{
		delete attract;
		attract = nullptr;
	}
	if (play)
	{
		delete play;
		play = nullptr;
	}
	if (hiscore)
	{
		delete hiscore;
		hiscore = nullptr;
	}
}

int Game::init(void)
{
	startMusic();
#ifdef _DEBUG
	//musicVolume(0);
#endif

	render = new Render(gameWindow);
	render->init();

	attract = new Attract();
	play = new Play();
	hiscore = new HiScore();

	switchMode(gamemode::Attract);

	return 0;
}

void Game::switchMode(gamemode newMode)
{
	if (state != nullptr)
		state->shutdown();

	mode = newMode;

	switch (mode)
	{
		case gamemode::Attract:
			state = attract;
			break;
		case gamemode::Play:
			state = play;
			break;
		case gamemode::HiScore:
			state = hiscore;
			break;
		default:
			assert(0);
	}

	state->init();
}
