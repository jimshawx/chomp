//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include <windows.h>
#include <gl/gl.h>
#include <assert.h>
#include <ctype.h>

#include <cmath>
#include <string>

#include "grid.h"
#include "player.h"
#include "utils.h"
#include "logger.h"
#include "dbgnew.h"
#include "ghost.h"

using namespace std;

extern void drawcapsule();

void Ghost::draw()
{
	switch (state)
	{
		case ghoststate::Scatter:
		case ghoststate::Waiting:
		case ghoststate::Freeze:
		case ghoststate::Chase:
			drawNormal();
			break;

		case ghoststate::Frightened:
			drawFrightened();
			break;

		case ghoststate::Returning:
			drawReturning();
			break;

		default:
			assert(0);
	}
}

void Ghost::move(int dt)
{
	//assert(!grid->isWall((int)ghostX, (int)ghostY));

	updateState(dt);

	switch (state)
	{
		case ghoststate::Scatter:
			moveScatter(dt);
			break;

		case ghoststate::Chase:
			moveChase(dt);
			break;

		case ghoststate::Waiting:
			moveWaiting(dt);
			break;

		case ghoststate::Frightened:
			moveFrightened(dt);
			break;

		case ghoststate::Returning:
			moveReturning(dt);
			break;

		case ghoststate::Freeze:
			break;

		default:
			assert(0);
	}

	collide();
}
bool Ghost::followTrack(int dt, float speed)
{
	//elog("%6s (%6.2f,%6.2f)->(%d,%d) @ (%6.2f,%6.2f)\n", getName().c_str(), ghostX, ghostY, targetX, targetY, dirX, dirY);

	float treacleScale = 1.0;
	if (grid->isTreacle((int)ghostX, (int)ghostY))
		treacleScale = treacleSpeed;

	float remainingSpeed = speed;

	for (int i = 0; i < 2; i++)
	{
		assert(dirX == 0.0f || dirX == 1.0f || dirX == -1.0f);
		assert(dirY == 0.0f || dirY == 1.0f || dirY == -1.0f);

		float oldX = ghostX;
		float oldY = ghostY;

		ghostX += (float)dt * remainingSpeed * dirX * speedScale * treacleScale;
		ghostY += (float)dt * remainingSpeed * dirY * speedScale * treacleScale;

		//assert(!grid->isWall((int)ghostX, (int)ghostY));

		//have we arrived at the target point?
		
		//if ((fabs((float)targetX - ghostX) > gridTolerance)
		//	|| (fabs((float)targetY - ghostY) > gridTolerance))
		//	return false;

		//if the new point is closer to target than old point, then keep going
		if (fabs(ghostX - targetX) <= (fabs(oldX - targetX)) &&
			fabs(ghostY - targetY) <= (fabs(oldY - targetY)))
			return false;

		float dx0 = (ghostX - targetX);
		float dy0 = (ghostY - targetY);
		//float dx1 = (oldX - targetX);
		//float dy1 = (oldY - targetY);*/
		//elog("(%6.2f %6.2f)->(%6.2f %6.2f)\n", dx0,dy0,dx1,dy1);

		remainingSpeed = fabs(dx0 + dy0);

	//we arrived at the target

	//clamp into the cell
		int cellX = targetX;
		int cellY = targetY;
		ghostX = (float)targetX;
		ghostY = (float)targetY;

		assert(!grid->isWall(cellX, cellY));

		if (needToFlipDirection)
		{
			applyFlipDirection();
			return false;
		}

		//is it hyperspace?
		int hx, hy;
		if (grid->isHyperspace(cellX, cellY, hx, hy))
		{
			ghostX = (float)hx + dirX;
			ghostY = (float)hy + dirY;
			updateTarget();
			return false;
		}

		//is it a corner or a junction?

		int count;
		unsigned int routemask = grid->junctions(cellX, cellY, count);
		assert(count >= 2);

		if (count > 2)
		{
			//junction

			//need to decide a new direction
			return true;
		}
		else if (count == 2)
		{
			//turn a corner

			//eliminate the direction we're coming from
			if (dirX == -1.0f)
				routemask &= ~2;
			if (dirX == 1.0f)
				routemask &= ~8;
			if (dirY == 1.0f)
				routemask &= ~1;
			if (dirY == -1.0f)
				routemask &= ~4;

			assert(routemask == 1 || routemask == 2 || routemask == 4 || routemask == 8);

			//set up the direction
			dirX = 0.0f;
			dirY = 0.0f;

			if (routemask == 1) dirY = -1.0f;
			if (routemask == 2) dirX = 1.0f;
			if (routemask == 4) dirY = 1.0f;
			if (routemask == 8) dirX = -1.0f;

			assert(dirX != 0.0f || dirY != 0.0f);
		}

		//todo: deal with any fractional movement remaining?

		//pick a new target
		updateTarget();
	}

	return false;
}


void Ghost::moveScatter(int dt)
{
	if (followTrack(dt, normalSpeed))
		decideScatter();
}

void Ghost::moveChase(int dt)
{
	if (followTrack(dt, normalSpeed))
		decideChase();
}

void Ghost::moveWaiting(int dt)
{
	//nothing
}

void Ghost::moveFrightened(int dt)
{
	if (followTrack(dt, ghostSpeed))
		decideFrightened();
}

void Ghost::moveReturning(int dt)
{
	if (followTrack(dt, returningSpeed))
		decideReturning();
}


void Ghost::drawNormal()
{
	float xx, yy, zz;
	grid->toXYZ(ghostX, ghostY, xx, yy, zz);

	xx += 15.0;
	yy += 15.0;

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glTranslatef(xx + 12.5f, yy + 12.5f, zz + 25.0f);
	glScalef(50.0f, 50.0f, 50.0f);
	glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	drawcapsule();

	drawEyes();

	glPopMatrix();
}

void Ghost::drawFrightened()
{
	float xx, yy, zz;
	grid->toXYZ(ghostX, ghostY, xx, yy, zz);

	xx += 15.0;
	yy += 15.0;

	if (frightenedTimer > 1000 || ((frightenedTimer / 100) & 1))
		glColor3f(0.3f, 0.3f, 1.0f);
	else
		glColor3f(1.0f, 1.0f, 1.0f);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(xx + 12.5f, yy + 12.5f, zz + 25.0f);
	glScalef(50.0f, 50.0f, 50.0f);
	glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	drawcapsule();
	drawEyes();
	glPopMatrix();
}

void Ghost::drawReturning()
{
	float xx, yy, zz;
	grid->toXYZ(ghostX, ghostY, xx, yy, zz);

	xx += 15.0f;
	yy += 15.0f;
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(xx + 12.5f, yy + 12.5f, zz + 25.0f);
	glScalef(50.0f, 50.0f, 50.0f);
	glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	drawEyes();
	glPopMatrix();

}

void Ghost::drawEyes()
{
	float xx, yy, zz;
	glBegin(GL_QUADS);

	zz = 0.7f;
	float eye = 0.3f;
	float pupil = 0.2f;

	if (state == ghoststate::Frightened)
		glColor3f(1.0f, 1.0f, 0.0f);
	else
		glColor3f(1.0f, 1.0f, 1.0f);

	xx = -0.5f + (0.5f - eye) / 2.0f;
	yy = 0.25f;
	glVertex3f(xx, yy, zz);
	glVertex3f(xx + eye, yy, zz);
	glVertex3f(xx + eye, yy + eye, zz);
	glVertex3f(xx, yy + eye, zz);

	xx = 0.0f + (0.5f - eye) / 2.0f;
	glVertex3f(xx, yy, zz);
	glVertex3f(xx + eye, yy, zz);
	glVertex3f(xx + eye, yy + eye, zz);
	glVertex3f(xx, yy + eye, zz);

	glColor3f(0.0f, 0.0f, 0.0f);

	float offx = 0.5f - pupil;
	if (dirX < 0.0f) offx = 0.0f;
	float offy = eye - pupil;
	if (dirY > 0.0f) offy = 0.0f;

	xx = -0.5f + offx;
	yy = 0.25f + offy;
	glVertex3f(xx, yy, zz);
	glVertex3f(xx + pupil, yy, zz);
	glVertex3f(xx + pupil, yy + pupil, zz);
	glVertex3f(xx, yy + pupil, zz);

	xx = 0.0f + offx;
	glVertex3f(xx, yy, zz);
	glVertex3f(xx + pupil, yy, zz);
	glVertex3f(xx + pupil, yy + pupil, zz);
	glVertex3f(xx, yy + pupil, zz);

	glEnd();
}

void Ghost::setState(ghoststate newstate)
{
	frightenedTimer = 0;
	waitTimer = 0;
	scatterTimer = 0;
	chaseTimer = 0;

	ghoststate oldstate = state;
	state = newstate;

	switch (state)
	{
		case ghoststate::Chase:
			chaseTimer = chaseTime;
			if (oldstate == ghoststate::Scatter)
				flipDirection();
			break;
		case ghoststate::Frightened:
			frightenedTimer = ghostTime;
			if (oldstate == ghoststate::Chase || oldstate == ghoststate::Scatter)
				flipDirection();
			break;
		case ghoststate::Returning:
			break;
		case ghoststate::Freeze:
			//dirX = 0.0f;
			//dirY = 0.0f;
			break;
		case ghoststate::Scatter:
			scatterTimer = scatterTime;
			if (oldstate == ghoststate::Chase)
				flipDirection();
			break;
		case ghoststate::Waiting:
			waitTimer = waitTime;
			break;
		default:
			assert(0);
	}
}

void Ghost::updateState(int dt)
{
	switch (state)
	{
		case ghoststate::Chase:
			chaseTimer -= dt;
			if (chaseTimer < 0)
				setState(ghoststate::Scatter);
			break;
		case ghoststate::Frightened:
			frightenedTimer -= dt;
			if (frightenedTimer < 0)
			{
				setState(ghoststate::Scatter);
				player->resetGhostMultiplier();
			}
			break;
		case ghoststate::Returning:
		case ghoststate::Freeze:
			break;
		case ghoststate::Scatter:
			scatterTimer -= dt;
			if (scatterTimer < 0)
				setState(ghoststate::Chase);
			break;
		case ghoststate::Waiting:
			waitTimer -= dt;
			if (waitTimer < 0)
			{
				setState(ghoststate::Scatter);

				dirY = 0.0f;
				dirX = -1.0f;

				int gx, gy;
				grid->ghostHome('b', gx, gy);
				ghostX = (float)gx;
				ghostY = (float)gy;

				updateTarget();
			}
			break;
		default:
			assert(0);
	}
}

void Ghost::decideReturning()
{
	int x, y;
	getWaitingHome(x, y);

	if ((int)ghostX == x && (int)ghostY == y)
	{
		setState(ghoststate::Waiting);
		return;
	}

	headForPosition(x, y);
}

void Ghost::decideFrightened()
{
	decideRandom();
}

void Ghost::collide()
{
	float playerX, playerY;
	player->getPixelPosition(playerX, playerY);
	if (fabs(playerX - ghostX) < 0.4f && fabs(playerY - ghostY) < 0.4f)
	{
		if (state == ghoststate::Frightened)
		{
			player->caughtGhost();
			setState(ghoststate::Returning);
		}
		else if (state == ghoststate::Chase || state == ghoststate::Scatter)
		{
			player->caughtByGhost();
		}
	}
}

void Ghost::updateTarget()
{
	assert(dirX == 0.0f || dirX == -1.0f || dirX == 1.0f);
	assert(dirY == 0.0f || dirY == -1.0f || dirY == 1.0f);

	assert((dirX == 0.0f && dirY != 0.0f) || (dirY == 0.0f && dirX != 0.0f));

	targetX = (int)ghostX + (int)dirX;
	targetY = (int)ghostY + (int)dirY;

	assert(!grid->isWall(targetX, targetY));
}

void Ghost::decideRandom()
{
	int count;
	unsigned int routemask = grid->junctions((int)ghostX, (int)ghostY, count);

	//eliminate the direction we're coming from
	if (dirX == -1.0f)
		routemask &= ~2;
	if (dirX == 1.0f)
		routemask &= ~8;
	if (dirY == 1.0f)
		routemask &= ~1;
	if (dirY == -1.0f)
		routemask &= ~4;

	assert(routemask != 0);

	for (;;)
	{
		unsigned int bit = 1 << (irand() % 4);
		if (!(routemask & bit)) continue;
		routemask &= bit;
		dirX = 0.0f;
		dirY = 0.0f;
		if (routemask == 1) dirY = -1.0f;
		if (routemask == 2) dirX = 1.0f;
		if (routemask == 4) dirY = 1.0f;
		if (routemask == 8) dirX = -1.0f;
		assert(dirX != 0.0f || dirY != 0.0f);

		break;
	}
	updateTarget();
}

void Ghost::headForPosition(int targetX, int targetY)
{
	int count;
	unsigned int routemask = grid->junctions((int)ghostX, (int)ghostY, count);

	//eliminate the direction we're coming from
	if (dirX == -1.0f)
		routemask &= ~2;
	if (dirX == 1.0f)
		routemask &= ~8;
	if (dirY == 1.0f)
		routemask &= ~1;
	if (dirY == -1.0f)
		routemask &= ~4;

	assert(routemask != 0);

	unsigned int direction = grid->getDirection((int)ghostX, (int)ghostY, targetX, targetY, routemask);
	if (direction == 0)
	{
		//we've arrived, so choose randomly where to go next
		decideRandom();
		return;
	}

	dirX = 0.0f;
	dirY = 0.0f;
	if (direction == 1) dirY = -1.0f;
	if (direction == 2) dirX = 1.0f;
	if (direction == 4) dirY = 1.0f;
	if (direction == 8) dirX = -1.0f;

	updateTarget();
}

//- INKY --------------------------------------------------------------------------------------------------------------

void Inky::drawNormal()
{
	//cyan
	glColor3ub(0, 255, 255);
	Ghost::drawNormal();
}

void Inky::decideChase()
{
	//original rule - pick 2 squares in front of player, head for point double the line from blinky through it

	//head for the junction/corner that player is headed for
	int x, y;
	player->getTarget(x, y);
	headForPosition(x, y);
}

void Inky::decideScatter()
{
	int x, y;
	grid->ghostHome(name[0], x, y);
	headForPosition(x, y);
}

string Inky::getName()
{
	return name;
}

void Inky::getWaitingHome(int &x, int &y)
{
	grid->ghostHome(tolower(name[0]), x, y);
}

//- BLINKY ------------------------------------------------------------------------------------------------------------

void Blinky::drawNormal()
{
	//red
	glColor3ub(255, 0, 0);
	Ghost::drawNormal();
}

void Blinky::decideChase()
{
	//chase player
	int x, y;
	player->getGridPosition(x, y);
	headForPosition(x, y);
}

void Blinky::decideScatter()
{
	int x, y;
	grid->ghostHome(name[0], x, y);
	headForPosition(x, y);
}

string Blinky::getName()
{
	return name;
}

void Blinky::getWaitingHome(int &x, int &y)
{
	grid->ghostHome(tolower(name[0]), x, y);
}

//- PINKY -------------------------------------------------------------------------------------------------------------

void Pinky::drawNormal()
{
	//pink
	glColor3ub(253, 153, 203);
	Ghost::drawNormal();
}

void Pinky::decideChase()
{
	//original rule is - head 4 squares in front of player

	//head for the junction/corner that player is headed for
	int x, y;
	player->getTarget(x, y);
	headForPosition(x, y);
}

void Pinky::decideScatter()
{
	int x, y;
	grid->ghostHome(name[0], x, y);
	headForPosition(x, y);
}

string Pinky::getName()
{
	return name;
}

void Pinky::getWaitingHome(int &x, int &y)
{
	grid->ghostHome(tolower(name[0]), x, y);
}

//- CLYDE -------------------------------------------------------------------------------------------------------------

void Clyde::drawNormal()
{
	//orange
	glColor3ub(255, 128, 0);
	Ghost::drawNormal();
}

void Clyde::decideChase()
{
	//chase the player if they are too close, otherwise go home
	int playerX, playerY;
	player->getGridPosition(playerX, playerY);

	int dx = playerX - (int)ghostX;
	int dy = playerY - (int)ghostY;
	int distance = dx * dx + dy * dy;
	if (distance < 64)
	{
		decideScatter();
	}
	else
	{
		//chase player
		int x, y;
		player->getGridPosition(x, y);
		headForPosition(x, y);
	}
}

void Clyde::decideScatter()
{
	int x, y;
	grid->ghostHome(name[0], x, y);
	headForPosition(x, y);
}

void Clyde::getWaitingHome(int &x, int &y)
{
	grid->ghostHome(tolower(name[0]), x, y);
}

string Clyde::getName()
{
	return name;
}
