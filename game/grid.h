//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include <vector>
#include <tuple>

#include "dbgnew.h"

using namespace std;

class Grid
{
public:
	~Grid()
	{
		if (grid)
		{
			delete grid;
			grid = nullptr;
		}
		hyperspace.clear();
		homes.clear();
	}

	void load(const char *grid);
	void draw();

	bool isWall(int x, int y);
	bool isHyperspace(int x, int y, int &hx, int &hy);
	bool isTreacle(int x, int y);
	unsigned int junctions(int x, int y, int &count);
	void home(int &x, int &y);
	void start(int &x, int &y);
	void ghostHome(char who, int &x, int &y);

	void toXYZ(float x, float y, float &xx, float &yy, float &zz);

	unsigned int getDirection(int fromX, int fromY, int targetX, int targetY, unsigned int available);
	bool isJunction(int x, int y);
	bool isCorner(int x, int y);
	int collect(int x, int y);
	bool complete();

private:
	int gridX = 0;
	int gridY = 0;

	vector<tuple<int, int>> hyperspace;
	vector <tuple<char, int, int>> homes;

	enum class gridtype
	{
		Blank=0,
		Wall,
	};
	enum class collectibletype
	{
		None=0,
		Pill,
		PowerPill,
		Hyperspace,
		Treacle,
	};

	class griditem
	{
	public:
		gridtype type = gridtype::Blank;
		collectibletype collectible = collectibletype::None;
	};

	griditem *grid = nullptr;

	const float dim = 50.0f;
	unsigned int gridScore = 0;
};
