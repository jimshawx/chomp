//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "glext.h"

#include <assert.h>

#include "render.h"
#include "logger.h"
#include "dbgnew.h"

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;
const int SCREEN_WIDTH = 1392;
const int SCREEN_HEIGHT = 768;
const int VIEW_ANGLE = 90;

int Render::init()
{
	PIXELFORMATDESCRIPTOR pixeldesc;
	int pixelformat;
	HDC hDC;

	hDC = GetDC(gameWindow);

	memset(&pixeldesc, 0, sizeof pixeldesc);
	pixeldesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pixeldesc.nVersion = 1;

	pixeldesc.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pixeldesc.iPixelType = PFD_TYPE_RGBA;
	pixeldesc.cColorBits = 32;

	pixeldesc.cRedBits = 0;
	pixeldesc.cGreenBits = 0;
	pixeldesc.cBlueBits = 0;
	pixeldesc.cAlphaBits = pixeldesc.cAlphaShift = 0;
	pixeldesc.cAccumBits = 32;
	pixeldesc.cAccumRedBits =
		pixeldesc.cAccumGreenBits =
		pixeldesc.cAccumBlueBits = 0;
	pixeldesc.cAccumAlphaBits = 0;

	pixeldesc.cDepthBits = 32;

	pixeldesc.cStencilBits = 0;
	pixeldesc.cAuxBuffers = 0;

	pixeldesc.bReserved = 0;
	pixeldesc.dwLayerMask = 0;
	pixeldesc.dwVisibleMask = 0;
	pixeldesc.dwDamageMask = 0;

	if ((pixelformat = ChoosePixelFormat(hDC, &pixeldesc)) == 0)
	{
		elog(LOG_LEVEL::LOG_ERROR, "ChoosePixelFormat failed\n", 0);
		return 1;
	}

	if (pixeldesc.dwFlags & PFD_NEED_PALETTE)
	{
		elog(LOG_LEVEL::LOG_ERROR, "Needs Palette\n", 0);
		return 1;
	}
	if (SetPixelFormat(hDC, pixelformat, &pixeldesc) == FALSE)
	{
		elog(LOG_LEVEL::LOG_ERROR, "SetPixelFormat failed", 0);
		return 1;
	}
	DescribePixelFormat(hDC, pixelformat, sizeof pixeldesc, &pixeldesc);

	gameRC = wglCreateContext(hDC);
	wglMakeCurrent(hDC, gameRC);
	ReleaseDC(gameWindow, hDC);

	const char *txt;
	txt = (const char *)glGetString(GL_VENDOR);
	if (txt)
		elog("Vendor %s\n", txt);
	txt = (const char *)glGetString(GL_RENDERER);
	if (txt)
		elog("Renderer %s\n", txt);
	txt = (const char *)glGetString(GL_VERSION);
	if (txt)
		elog("Version %s\n", txt);
	txt = (const char *)glGetString(GL_EXTENSIONS);
	elog("Extensions %s\n", txt);

	GLint v;
	glGetIntegerv(GL_MAX_LIGHTS, &v);
	elog("Max HW lights %d\n", v);

	defaultstate();

	if (strstr((const char *)glGetString(GL_EXTENSIONS), "WGL_EXT_swap_control"))
	{
		wglSwapIntervalEXT = reinterpret_cast<void (APIENTRY *)(int)>(wglGetProcAddress("wglSwapIntervalEXT"));
		if (wglSwapIntervalEXT)
			wglSwapIntervalEXT(1);
	}

	return 0;
}

void Render::shutdown()
{
	wglMakeCurrent(NULL, NULL);

	if (gameRC)
		wglDeleteContext(gameRC);
	gameRC = nullptr;
}

void Render::defaultstate(void)
{
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

	glDepthRange(0.0, 1.0);
	glClearDepth(1.0);
	glClearColor(0.5, 0.5, 0, 1.0);

	glShadeModel(GL_SMOOTH);

	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);

	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	glDisable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glEnable(GL_TEXTURE_2D);
}

void Render::flip(void)
{
	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
		elog(LOG_LEVEL::LOG_ERROR, "GLERROR %08X %d\n", err, err);

	HDC dc = GetDC(gameWindow);
	SwapBuffers(dc);
	ReleaseDC(gameWindow, dc);
}

void Render::cls(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Render::mode3d(void)
{
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective((VIEW_ANGLE * SCREEN_HEIGHT) / SCREEN_WIDTH, SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.125, 32768.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Render::modeHud(void)
{
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0.0, (GLdouble)SCREEN_WIDTH, (GLdouble)SCREEN_HEIGHT, 0.0, 0.0, 1.0);
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}
