//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#include <windows.h>
#include <gl/gl.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <tuple>
#include <queue>
#include <functional>

#include <ctype.h>
#include <assert.h>

using namespace std;

#include "grid.h"
#include "logger.h"
#include "dbgnew.h"

void Grid::load(const char *gridfile)
{
	string path = string(gridfile);
	path = "grids/" + path;
	
	vector<string> lines;

	{
		ifstream f;
		f.open(path);
		string line;
		while (getline(f, line))
		{
			if (line == "^Z") break;
			if (line[0] == '@') continue;
			if (all_of(line.begin(), line.end(), isspace)) continue;
			lines.push_back(line);
		}
		f.close();
	}

	gridY = (int)lines.size();
	gridX = 0;
	for (auto e : lines)
		gridX = max(gridX, (int)e.size());

	grid = new griditem[(size_t)gridX * gridY];
	memset(grid, 0, (size_t)gridX * gridY * sizeof * grid);

	griditem *g = grid;
	for (int y = 0; y < gridY; y++)
	{
		for (int x = 0; x < (int)lines[y].size(); x++)
		{
			switch (lines[y][x])
			{
				case '#':
					g->type = gridtype::Wall;
					break;
				case '.': 
					g->collectible = collectibletype::Pill;
					break;
				case 'o':
					g->type = gridtype::Blank;
					g->collectible = collectibletype::PowerPill;
					break;
				case '=':
					g->collectible = collectibletype::Hyperspace;
					hyperspace.push_back({ x,y });
					break;
				
					homes.push_back({ lines[y][x],x,y });
					break;
				case '-':
					g->type = gridtype::Blank;
					break;
				case ' ':
					g->type = gridtype::Blank;
					break;
				case 'T':
					g->collectible = collectibletype::Treacle;
					break;
				case 'x':
					homes.push_back({ lines[y][x],x,y });
					break;
				case 'I':
				case 'B':
				case 'P':
				case 'C':
					g->collectible = collectibletype::Pill;
					homes.push_back({ lines[y][x],x,y });
					break;
				case 'i':
				case 'b':
				case 'p':
				case 'c':
					homes.push_back({ lines[y][x],x,y });
					break;
				default:
					//unknown cell type
					assert(0);
			}
			if (g->collectible == collectibletype::PowerPill || g->collectible == collectibletype::Pill)
				gridScore++;
			g++;
		}
		g += gridX - lines[y].size();
	}

	assert(hyperspace.size() == 0 || hyperspace.size() == 2);
	assert(homes.size() == 9);
}

void Grid::draw()
{
	float xx, yy, zz;

	glDisable(GL_TEXTURE_2D);

	glBegin(GL_QUADS);
	for (int y = 0; y < gridY; y++)
	{
		for (int x = 0; x < gridX; x++)
		{
			toXYZ((float)x, (float)y, xx, yy, zz);

			switch (grid[x + y * gridX].type)
			{
				case gridtype::Blank:
					glColor3f(0.2f, 0.2f, 0.2f);

					glVertex3f(xx, yy, zz);
					glVertex3f(xx + dim, yy, zz);
					glVertex3f(xx + dim, yy + dim, zz);
					glVertex3f(xx, yy + dim, zz);
					break;

				case gridtype::Wall:
					glColor3f(0.5f, 0.5f, 1.0f);

					glVertex3f(xx, yy, zz+25.0f);
					glVertex3f(xx + dim, yy, zz + 25.0f);
					glVertex3f(xx + dim, yy + dim, zz + 25.0f);
					glVertex3f(xx, yy + dim, zz + 25.0f);
					break;
			}

			switch (grid[x + y * gridX].collectible)
			{
				case collectibletype::PowerPill:
					glColor3f(1.0f, 1.0f, 1.0f);

					xx += 10.0f;
					yy += 10.0f;
					glVertex3f(xx, yy, zz + 25.0f);
					glVertex3f(xx + 30.0f, yy, zz + 25.0f);
					glVertex3f(xx + 30.0f, yy + 30.0f, zz+25.0f);
					glVertex3f(xx, yy + 30.0f, zz + 25.0f);
					break;

				case collectibletype::Pill:
					glColor3f(1.0f, 1.0f, 0.0f);

					xx += 20.0f;
					yy += 20.0f;
					glVertex3f(xx, yy, zz + 25.0f);
					glVertex3f(xx + 10.0f, yy, zz + 25.0f);
					glVertex3f(xx + 10.0f, yy + 10.0f, zz + 25.0f);
					glVertex3f(xx, yy + 10.0f, zz + 25.0f);
					break;
			}

		}
	}
	
	glEnd();
}

bool Grid::isWall(int x, int y)
{
	if (x < 0 || x >= gridX) return true;//outside the grid is wall, to allow for hyperspace tunnel
	if (y < 0 || y >= gridY) return true;
	return grid[x + y * gridX].type == gridtype::Wall;
}

bool Grid::isTreacle(int x, int y)
{
	if (x < 0 || x >= gridX) return false;
	if (y < 0 || y >= gridY) return false;
	return grid[x + y * gridX].collectible == collectibletype::Treacle;
}

bool Grid::isHyperspace(int x, int y, int &hx, int &hy)
{
	if (x < 0 || x >= gridX) return false;
	if (y < 0 || y >= gridY) return false;

	if (grid[x + y * gridX].collectible == collectibletype::Hyperspace)
	{
		tuple<int, int> h0, h1;
		h0 = hyperspace[0];
		h1 = hyperspace[1];
		if (get<0>(h0) == x && get<1>(h0) == y)
		{
			hx = get<0>(h1);
			hy = get<1>(h1);
		}
		else
		{
			hx = get<0>(h0);
			hy = get<1>(h0);
		}
		return true;
	}
	hx = 0;
	hy = 0;
	return false;
}

unsigned int Grid::junctions(int x, int y, int &count)
{
	count = 0;
	if (x < 1 || x >= gridX - 1) return 0;
	if (y < 1 || y >= gridY - 1) return 0;

	unsigned int junc = 0;
	if (grid[x + y * gridX].type == gridtype::Blank)
	{
		if (y>=0 && grid[x + (y - 1) * gridX].type == gridtype::Blank) { junc |= 1; count++; }
		if (x<gridX-1 && grid[(x+1) + y * gridX].type == gridtype::Blank) { junc |= 2; count++; }
		if (y<gridY-1 && grid[x + (y+1) * gridX].type == gridtype::Blank) { junc |= 4; count++; }
		if (x>=0 && grid[(x-1) + y * gridX].type == gridtype::Blank) { junc |= 8; count++; }
	}
	return junc;
}

bool Grid::isJunction(int x, int y)
{
	int count = 0;
	junctions(x, y, count);
	return count > 2;
}

bool Grid::isCorner(int x, int y)
{
	int count = 0;
	junctions(x, y, count);
	return count > 1;
}

void Grid::toXYZ(float x, float y, float &xx, float &yy, float &zz)
{
	float hx = (float)gridX * dim * 0.5f;
	float hy = (float)gridY * dim * 0.5f;
	float hz = -max(hx, hy) * 2.0f;

	xx = (float)x * dim - hx;
	yy = -(float)y * dim + hy;
	zz = 0.0f;// hz;
}

void Grid::home(int &x, int &y)
{
	ghostHome('H',x,y);
}

void Grid::start(int &x, int &y)
{
	ghostHome('x', x, y);
}

void Grid::ghostHome(char whoOrWhat, int &x, int &y)
{
	for (auto e : homes)
	{
		if (get<0>(e) == whoOrWhat)
		{
			x = get<1>(e);
			y = get<2>(e);
			return;
		}
	}
	//don't have a home 
	assert(0);
}

unsigned int Grid::getDirection(int fromX, int fromY, int targetX, int targetY, unsigned int available)
{
	assert(available != 0);

	//elog("planning a route from (%d,%d) to (%d,%d) %u\n", fromX, fromY, targetX, targetY, available);

	//starting at from and heading for target, which direction should I move out of those available?
	
	//we're at the target, there's nothing to do
	if (targetX == fromX && targetY == fromY)
		return 0;

	assert(!isWall(targetX, targetY));

	//breadth first search, with backtrack to find first junction

	class cell
	{
	public:
		int x, y;
		cell *prev;
		bool visited;
		bool routed;
	};
	cell *cells = new cell[(size_t)gridX * gridY];
	{
		cell *c = cells;
		for (int y = 0; y < gridY; y++)
		{
			for (int x = 0; x < gridX; x++)
			{
				c->x = x;
				c->y = y;
				c->prev = nullptr;
				c->visited = false;
				c->routed = false;
				c++;
			}
		}
	}

	queue<cell *> targets;

	auto allocatecell = [cells, this](int x, int y, cell *prev)->cell *
	{
		assert(x >= 0 && x < gridX);
		assert(y >= 0 && y < gridY);

		//elog("queueing %d %d\n", x, y);

		cell *c = &cells[x + y * gridX];

		assert(c->x == x && c->y == y);

		c->prev = prev;
		c->visited = true;
		return c;
	};
	auto isVisited = [cells, this](int x, int y)->bool
	{
		return cells[x + y * gridX].visited;
	};

	//mark the start as visited
	cell *start = allocatecell(fromX, fromY, nullptr);
	start = nullptr;

	//prime the queue with some points
	if (available & 1) targets.push(allocatecell(fromX, fromY - 1, start));
	if (available & 2) targets.push(allocatecell(fromX+1, fromY, start));
	if (available & 4) targets.push(allocatecell(fromX, fromY + 1, start));
	if (available & 8) targets.push(allocatecell(fromX-1, fromY, start));

	for (;;)
	{
		assert(!targets.empty());

		cell *c = targets.front();
		targets.pop();

		//elog("looking at %d %d\n", c->x, c->y);

		if (c->x == targetX && c->y == targetY)
		{
			//backtrack to first corner, return its direction from start
			int cornerX=-1, cornerY;
			do
			{
				c->routed = true;
				if (isCorner(c->x, c->y))
				{
					cornerX = c->x;
					cornerY = c->y;
				}
				c = c->prev;
			} while (c);
			assert(cornerX != -1);

			//elog("heading for junction at (%d,%d)\n", cornerX, cornerY);
			if (0)
			{
				cell *c = cells;
				for (int y = 0; y < gridY; y++)
				{
					for (int x = 0; x < gridX; x++)
					{
						if (isWall(x, y))
							elog("#");
						else if (c->routed)
							elog(".");
						else
							elog(" ");
						c++;
					}
					elog("\n");
				}
				elog("\n");
			}

			//cleanup
			delete[] cells;
			cells = nullptr;

			//the junction will be on the same row or column as the from
			if (cornerX == fromX)
			{
				//above or below
				if (cornerY > fromY)
					return 4;
				return 1;
			}
			else if (cornerY == fromY)
			{
				//left or right
				if (cornerX > fromX)
					return 2;
				return 8;
			}
			else
			{
				assert(0);
			}
			
			return 0;
		}

		//add the possible (non wall, non visited) neighbouring cells to the queue
		static const int xoff[] = { 0,1,0,-1 };
		static const int yoff[] = { -1,0,1,0 };
		for (int i = 0; i < 4; i++)
		{
			int xx = c->x + xoff[i];
			int yy = c->y + yoff[i];

			/*int hx, hy;
			if (isHyperspace(xx, yy, hx, hy))
			{
				allocatecell(xx, yy, c);
				xx = hx;
				yy = hy;
			}*/

			if (!isWall(xx, yy) && !isVisited(xx,yy))
				targets.push(allocatecell(xx, yy, c));
		}
	}
}

int Grid::collect(int x, int y)
{
	griditem *item = &grid[x + y * gridX];

	if (item->collectible == collectibletype::Pill)
	{
		item->collectible = collectibletype::None;
		gridScore--;
		return 1;
	}
	if (item->collectible == collectibletype::PowerPill)
	{
		item->collectible = collectibletype::None;
		gridScore--; 
		return 2;
	}
	return 0;
}

bool Grid::complete()
{
	return gridScore == 0;
}