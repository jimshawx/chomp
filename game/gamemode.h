//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

enum class gamemode
{
	Attract,
	HiScore,
	Play,
};

class GameMode
{
public:
	virtual ~GameMode() = default;

	virtual void init() = 0;
	virtual void shutdown() = 0;

	virtual void draw3d() =0;
	virtual void drawhud() = 0;
	virtual gamemode update(int dt) = 0;
}; 
