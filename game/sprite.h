//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include <windows.h>
#include <gl/gl.h>

class Sprite
{
private:
	unsigned int *pix = nullptr;
	int width = 0;
	int height = 0;
	bool istrans = false;
	unsigned int texture = 0;

public:
	Sprite(const wchar_t *filename, bool trans = false);
	~Sprite();

	void setTexture();
	void draw(int x, int y);
	void drawString(int x, int y, const char *fmt, ...);
	int extentString(const char *fmt, ...);
private:
	void drawChar(char c, int x, int y);
};
