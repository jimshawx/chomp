//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

enum class LOG_LEVEL
{
	LOG_INFO,
	LOG_WARN,
	LOG_ERROR
} ; 

void elog(const char *fmt, ...);
void elog(LOG_LEVEL level, const char *fmt, ...);