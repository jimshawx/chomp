//-----------------------------------------------------------------------------------------------------------------
// Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

#include "sprite.h"
#include "gamemode.h"

class Attract : public GameMode
{
public:
	Attract();
	~Attract();

	virtual void init();
	virtual void shutdown();

	virtual void draw3d();
	virtual void drawhud();

	virtual gamemode update(int dt);
private:
	Sprite *font = nullptr;
	float rainbowTimer = 0.0f;
	const float rainbowSpeed = 0.01f;

	void drawEyes();
};
