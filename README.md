# Chomp!

## A clone of a popular video game by Jim

Contains [fast_mongo.xm](https://modarchive.org/index.php?request=view_by_moduleid&query=41460), a soundtracker module by Johan Wilinger.
[License](https://modarchive.org/index.php?faq-licensing)  
Contains [madness_final.xm](https://modarchive.org/index.php?request=view_by_moduleid&query=148804), a soundtracker module by [raina](https://modarchive.org/index.php?request=view_profile&query=80599).
[License](https://modarchive.org/index.php?faq-licensing)

Released under the MIT License.

>Copyright 2020 James Shaw. All Rights Reserved.
>
>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.